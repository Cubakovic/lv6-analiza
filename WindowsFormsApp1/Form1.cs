﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        double x, y, rez;
        
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Krivi unos varijable x!", "Pogreška");
            }
            if (!double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Krivi unos varijable y!", "Pogreška");
            }
            rez = x + y;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Krivi unos varijable x!", "Pogreška");
            }
            if (!double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Krivi unos varijable y!", "Pogreška");
            }
            rez = x - y;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Krivi unos varijable x!", "Pogreška");
            }
            if (!double.TryParse(textBox2.Text, out y))
            {
                MessageBox.Show("Krivi unos varijable y!", "Pogreška");
            }
            rez = x * y;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Krivi unos varijable x!", "Pogreška");
            }
            if (!double.TryParse(textBox2.Text, out y) || y == 0)
            {
                MessageBox.Show("Krivi unos varijable y!", "Pogreška");
            }
            rez = x / y;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Krivi unos varijable x!", "Pogreška");
            }
            rez = Math.Sin(x);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Krivi unos varijable x!", "Pogreška");
            }
            rez = Math.Cos(x);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Krivi unos varijable x!", "Pogreška");
            }
            rez = Math.Log(x);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Krivi unos varijable x!", "Pogreška");
            }
            rez = Math.Exp(x);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Krivi unos varijable x!", "Pogreška");
            }
            rez = Math.Sqrt(x);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            textBox3.Text = rez.ToString();
            textBox3.Show();
            textBox1.Clear();
            textBox2.Clear();
        }

    }
}
